export function curry_v_2_1(fn) {
	return function curry() {
		const arg = Array.prototype.slice.call(arguments);
		if (arg.length >= fn.length) {
			return fn.apply(null, arg);
		}

		return function () {
			const newArg = Array.prototype.slice.call(arguments);
			return curry.apply(null, arg.concat(newArg));
		};
	};
}

// ↓ преобразует arguments в массив
// Array.prototype.slice.call(arguments)
// ↑ Object.values(arguments) ← аналог
