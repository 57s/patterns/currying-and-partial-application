export function curry_v_2(fn) {
	return function curry(...arg) {
		if (arg.length >= fn.length) {
			return fn(...arg);
		}

		return function (...newArg) {
			return curry(...arg, ...newArg);
		};
	};
}
