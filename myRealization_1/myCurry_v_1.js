export function myCurry_v_1() {
	let argumentAccumulator = [];

	return function accumulateArguments(...arg) {
		argumentAccumulator = [...argumentAccumulator, ...arg];

		accumulateArguments.valueOf = () => {
			return argumentAccumulator.reduce((acc, arg) => (acc += arg), 0);
		};

		return accumulateArguments;
	};
}
