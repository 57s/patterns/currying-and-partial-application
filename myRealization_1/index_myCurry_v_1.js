import { add } from '../utils/utils.js';
import { myCurry_v_1 } from './myCurry_v_1.js';
import { myCurry_v_1_2 } from './myCurry_v_1_2.js';

const curryAdd = myCurry_v_1_2(add);
const output = +curryAdd(10, 20, 20)(10, 40); // 100

console.log(output);
