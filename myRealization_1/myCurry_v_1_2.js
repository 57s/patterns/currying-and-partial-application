export function myCurry_v_1_2() {
	let argumentAccumulator = [];

	return function accumulateArguments(...arg) {
		argumentAccumulator = [...argumentAccumulator, ...arg];

		accumulateArguments.toString = () => {
			let result = 0;

			for (let index = 0; index < argumentAccumulator.length; index++) {
				const number = argumentAccumulator[index];
				result += number;
			}

			return result;
		};

		return accumulateArguments;
	};
}
