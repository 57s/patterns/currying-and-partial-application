import { add } from '../utils/utils.js';
import { myCurry_v_0 } from './myCurry_v_0.js';

const curryAdd = myCurry_v_0(add);
const output = curryAdd(10, 20, 20)(10, 40).getResult(); // 100

console.log(output);
