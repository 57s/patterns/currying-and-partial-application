export function myCurry_v_0(func) {
	let totalArgs = [];

	const batteryFunc = (...args) => {
		totalArgs = [...totalArgs, ...args];

		return batteryFunc;
	};

	batteryFunc.getResult = () =>
		totalArgs.reduce((currentResult, arg) => func(currentResult, arg), 0);

	return batteryFunc;
}
