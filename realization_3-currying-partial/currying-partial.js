export const partial = (fn, ...par) => {
	const curry = (...args) => {
		if (fn.length > args.length) {
			return partial(fn.bind(null, ...args));
		} else {
			return fn(...args);
		}
	};

	return par.length ? curry(...par) : curry;
};
