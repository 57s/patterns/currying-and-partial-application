import { partial } from './partial.js';
import { addABC } from '../utils/utils.js';

const withAB = partial(addABC, 10, 40);
const result = withAB(5);

console.log(result);
