// prettier-ignore
export const partial = (fn, ...arg) => (...rest) => fn(...arg.concat(rest));
