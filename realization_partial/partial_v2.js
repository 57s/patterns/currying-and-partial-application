export function partial_v2(func, ...partialArguments) {
	return function (...otherArguments) {
		return func(...partialArguments, ...otherArguments);
	};
}
