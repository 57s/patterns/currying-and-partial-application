export function curry_v_1(fn) {
	return function curry(...arg) {
		if (arg.length >= fn.length) {
			return fn.apply(this, arg);
		}

		return curry.bind(this, ...arg);
	};
}
