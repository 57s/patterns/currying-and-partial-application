import { addABC } from '../utils/utils.js';
import { curry_v_1 } from './curry_v_1.js';

const curryAdd = curry_v_1(addABC);
const output_v1 = curryAdd(10, 20)(20); // 50
const output_v2 = curryAdd(10)(40)(50); // 100

console.log(output_v1);
console.log(output_v2);
